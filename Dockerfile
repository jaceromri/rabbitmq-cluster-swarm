FROM rabbitmq:management-alpine

RUN apk --no-cache --update add netcat-openbsd

COPY entrypoint.sh /sbin/
RUN chmod +x /sbin/entrypoint.sh

CMD ["/sbin/entrypoint.sh"]
